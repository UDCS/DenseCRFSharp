# DenseCRFSharp

#### 介绍
denseCRF模型 是高效的全连接条件随机场模型进行图像语义分割
在深度学习中有着一定的作用。
斯坦福的2011年NIPS论文《Efficient Inference in Fully Connected CRFs with
Gaussian Edge Potentials》，阐述了如何使用高效的全连接条件随机场模型进行图像语义分割任务的应用。

根据文献资料，以及各位机器学习大神的文章讲解，参考结合开源PY或C源码，编写的基于C#的 DenseCRF。
全连接条件随机场大都是PY或者C，C#应用研究比较麻烦，C#版本便于C#理解研究应用。
DenseCRF 源代码与理论支持来自于
http://graphics.stanford.edu/projects/densecrf/

因为是边学习变制作，目前仅实现了关于 DenseCRF2D的部分内容，能够完整运行和方便对与基本逻辑的理解。

#### CRF和denseCRF
Conditional Random Fields(CRF)用于pixel-wise的图像标记（其实就是图像分割）。CRF经常用于 pixel-wise的label 预测。当把像素的label作为形成马尔科夫场随机变量且能够获得全局观测时，CRF便可以对这些label进行建模。
基本的CRF模型是由一阶势函数和相邻元素构成的势函数所组成的图模型，很显然，在图像任务上，CRF模型一个劣势就是它只考虑了相邻的邻域元素，没有对整体进行考虑。
全连接条件随机场(DenseCRF)
对于每个像素ii具有类别标签xixi还有对应的观测值yiyi，这样每个像素点作为节点，像素与像素间的关系作为边，即构成了一个条件随机场。而且我们通过观测变量yiyi来推测像素ii对应的类别标签xixi。条件随机场如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0102/182457_c703933e_598831.jpeg "20160904201421377.jpg")


#### 示例与使用方法
 `

           float[,,] im1=  util.readpng("im1.png");//预测图片
           float[,,] anno1 = util.readpng("anno1.png");//标记图片
           float[,,] unary = util.classify(anno1,3);//创建初始概率分布
            DenseCRF cRF = new DenseCRF(anno1.GetLength(0), anno1.GetLength(1),3);
            cRF.setUnaryEnergy(unary);
            cRF.addPairwiseGaussian(3, 3, 3);
            //cRF.addPairwiseGaussian(9, 9, 9);
            cRF.addPairwiseBilateral(60, 60, 20, 20, 20, im1, 30);
            short[,] map= cRF.map(10,1);
            //输出分类结果
            int w = map.GetLength(0);
            int h = map.GetLength(1);
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    Console.Write(map[i, j]);

                }
                Console.WriteLine("");
            }
`
