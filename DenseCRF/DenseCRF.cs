﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DenseCRF
{
   public class DenseCRF
    {
        float[,,] unary_ ;
        float[,,] additional_unary_ ;
        float[,,] current_;
        float[,,] next_ ;
        float[,,] tmp_ ;
        int W,  H, M;
        List<PairwisePotential> pairwise_ = new List<PairwisePotential>();
        public DenseCRF(int _W, int _H,int _M)
        {
            W = _W;
            H = _H;
            M = _M;
            unary_ = new float[W,H, M];
           additional_unary_ = new float[W, H, M];
           current_ = new float[W, H, M];
           next_ = new float[W, H, M];
           tmp_ = new float[W*2, H*2, M]; 
        }
        public void setUnaryEnergy(float[,,] unary)
        {
            unary_ = unary;
        }
      public   void addPairwiseGaussian(float sx, float sy, float w)
        {

            float[,,] feature = new float[W, H, 2];
            for (int j = 0; j < H; j++)
                for (int i = 0; i < W; i++)
                {
                    feature[i, j, 0] = i / sx;
                    feature[i, j, 1] = j / sy;
                }
            addPairwiseEnergy(feature, 2, w);

        }
        public void addPairwiseBilateral(float sx, float sy, float sr, float sg, float sb, float[,,] im, float w)
        {
            float[,,] feature = new float[W, H, 5];
            for (int j = 0; j < H; j++)
                for (int i = 0; i < W; i++)
                {
                    feature[i, j, 0] = i / sx;
                    feature[i, j, 1] = j / sy;
                    feature[i, j, 2] = im[i, j, 2] / sr;
                    feature[i, j, 3] = im[i, j, 1] / sg;
                    feature[i, j, 3] = im[i, j, 0] / sb;
                }
            //  float* feature = new float[N_ * 5];
            //for(int j=0; j<H_; j++ )
            //	for(int i=0; i<W_; i++ ){
            //		feature[(j * W_ + i) * 5 + 0] = i / sx;
            //		feature[(j * W_ + i) * 5 + 1] = j / sy;
            //		feature[(j * W_ + i) * 5 + 2] = im[(i + j * W_) * 3 + 0] / sr;
            //		feature[(j * W_ + i) * 5 + 3] = im[(i + j * W_) * 3 + 1] / sg;
            //		feature[(j * W_ + i) * 5 + 4] = im[(i + j * W_) * 3 + 2] / sb;
            //	}
            addPairwiseEnergy(feature, 5, w);
            // delete[] feature;
        }
        void stepInference(float relax)
        {


            for (int j = 0; j < H; j++)
            {
                for (int i = 0; i < W; i++)
                {
                    for (int s = 1; s < M; s++)
                    {
                        next_[i,j,s] = -unary_[i,j,s] - additional_unary_[i,j,s];
                    }
                }
            }

                   
             


            // Add up all pairwise potentials
            for (int i = 0; i < pairwise_.Count; i++)
                pairwise_[i].Apply(next_, current_, tmp_, M);

            // Exponentiate and normalize
            expAndNormalize(current_, next_, 1.0f, relax);
        }

        private void expAndNormalize(float[,,] out1, float[,,] in1, float scale=1, float relax=1)
        {
            float[,,] V = new float[W, H,M];
            for (int j = 0; j < H; j++)
            {
                for (int i = 0; i < W; i++)
                {
                    float b = in1[i,j,0];
                    float mx = scale * b;
                    for (int js = 1; js < M; js++)
                        if (mx < scale * in1[i, j, js])
                            mx = scale * in1[i, j, js];
                    float tt = 0;
                    for (int js = 0; js < M; js++)
                    {
                        V[i,j,js] = fast_exp(scale * in1[i, j, js] - mx);
                        tt += V[i,j,js];
                    }
                    for (int js = 0; js < M; js++)
                        V[i, j, js] /= tt;


                  
                    for (int js = 0; js < M; js++)
                        if (relax == 1)
                            out1[i,j,js] = V[i,j,js];
                        else
                            out1[i,j,js] = (1 - relax) * out1[i,j,js] + relax * V[i, j, js];

                }
            }
        
        }
        float fast_exp(float x)
        {
            bool lessZero = true;
            if (x < 0)
            {

                lessZero = false;
                x = -x;

            }
            // This diry little trick only works because of the normalization and the fact that one element in the normalization is 1
            if (x > 20)
                return 0;
            int mult = 0;

            while (x > 0.69 * 2 * 2 * 2)
            {

                mult += 3;
                x /= 8.0f;
            }

            while (x > 0.69 * 2 * 2)
            {
                mult += 2;

                x /= 4.0f;
            }
            while (x > 0.69)
            {

                mult++;
                x /= 2.0f;
            }

            x = very_fast_exp(x);
            while (mult>0)
            {

                mult--;
                x = x * x;
            }

            if (lessZero)
            {
                return 1 / x;

            }
            else
            {
                return x;
            }
        }
        float very_fast_exp(float x)
        {
            // err <= 3e-3
            // 	return 1
            // 		   -x*(0.9664
            // 		   -x*(0.3536));

            return 1
            - x * (0.9999999995f
            - x * (0.4999999206f

            - x * (0.1666653019f
            - x * (0.0416573475f
            - x * (0.0083013598f

            - x * (0.0013298820f
            - x * (0.0001413161f)))))));
        }
        float[,,] runInference(int n_iterations, float relax)
        {
            expAndNormalize(current_, unary_, -1);
            for (int it = 0; it < n_iterations; it++)
                stepInference(relax);
            return current_;
        }
        public   short[,] map(int n_iterations,  float relax=1)
        {
            // Run inference
            float[,,] prob = runInference(n_iterations, relax);
            short[,] result = new short[W, H];
            // Find the map
            for (int j = 0; j < H; j++)
                for (int i = 0; i < W; i++)
                {
                    int imx = 0;
                    float mx = prob[i, j, 0];
                    for (int s = 1; s < M; s++)
                    {
                        // Find the max and subtract it so that the exp doesn't explode 
                        if (mx < prob[i, j, s])
                        {
                            mx = prob[i, j, s];
                            imx = s;
                        }
                    }
                    result[i,j] = (short)imx;
                }
            return result;
        }

        private void addPairwiseEnergy(float[,,] feature, int D, float w)
        {
            PairwisePotential potts = new PairwisePotential(feature, D, W, H, w);
            pairwise_.Add(potts);
        }



    }


  public  class PottsPotential 
    {
        permutohedral lattice_=new permutohedral();
        int W, H;
        float[,,] norm_;
        public  PottsPotential(float[,,] features, int D, int _W,int _H, float w, bool per_pixel_normalization = true)
        {
            W = _W;
            H = _H;
            w_ = w;
            int N = W + H;
            lattice_.init(features, D, W,H);
            int value_size = 1;
            norm_ = new float[W, H, value_size];
            for (int i = 0; i < H; i++)
                for (int j = 0; j < W; j++)
                    for (int k = 0; k < value_size; k++)
                        norm_[j,i,k] = 1;
            //// Compute the normalization factor
            lattice_.compute(norm_, norm_, value_size,0,0,W,H);
            if (per_pixel_normalization)
            {
                // use a per pixel normalization
                for (int i = 0; i < H; i++)
                    for (int j = 0; j < W; j++)
                        for (int k = 0; k < value_size; k++)
                            norm_[j, i, k]  = 1.0f / (norm_[j, i, k]  + 1e-20f);
            }
            else
            {
                float mean_norm = 0;
                for (int i = 0; i < H; i++)
                    for (int j = 0; j < W; j++)
                        for (int k = 0; k < value_size; k++)
                            mean_norm += norm_[j, i, k];

                mean_norm = N / mean_norm;
                // use a per pixel normalization
                for (int i = 0; i < H; i++)
                    for (int j = 0; j < W; j++)
                        for (int k = 0; k < value_size; k++)
                              norm_[j, i, k]= mean_norm;
              
            }
        }
        float w_;
        public unsafe void Apply(float[,,] out_values, float[,,] in_values, float[,,] tmp, int value_size)
        {
            lattice_.compute(tmp, in_values, value_size, 0, 0, W, H);
            for (int i = 0; i < H; i++)
                for (int j = 0; j < W; j++)
                    for (int k = 0; k < value_size; k++)
                    {
                        out_values[j, i, k] = w_ * norm_[j, i, 0] * tmp[j, i, k];
                    }

        }
    }
    public class PairwisePotential : PottsPotential
    {
        public PairwisePotential(float[,,] features, int D, int _W, int _H, float w, bool per_pixel_normalization = true): base(features,  D,  _W,  _H,  w,  per_pixel_normalization = true)
        {
            
        }
    }
    class SemiMetricFunction
    {
        // For two probabilities apply the semi metric transform: v_i = sum_j mu_ij u_j
        void apply(float out_values, float in_values, int value_size) 
        { 
        }
    }
}
