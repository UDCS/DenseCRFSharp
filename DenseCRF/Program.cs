﻿using System;

namespace DenseCRF
{
    class Program
    { 

        static void Main(string[] args)
        {

           float[,,] im1=  util.readpng("im1.png");
           float[,,] anno1 = util.readpng("anno1.png");
           float[,,] unary = util.classify(anno1,3);

            DenseCRF cRF = new DenseCRF(anno1.GetLength(0), anno1.GetLength(1),3);
            cRF.setUnaryEnergy(unary);
            cRF.addPairwiseGaussian(3, 3, 3);
            //cRF.addPairwiseGaussian(9, 9, 9);
            cRF.addPairwiseBilateral(60, 60, 20, 20, 20, im1, 30);
            short[,] map= cRF.map(10,1);
            int w = map.GetLength(0);
            int h = map.GetLength(1);
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    Console.Write(map[i, j]);

                }
                Console.WriteLine("");
            }
            //using (var session = new TFSession())
            //{
            //    var graph = session.Graph;
            //    Console.WriteLine(TFCore.Version);
            //    var a = graph.Const(2);
            //    var b = graph.Const(3);
            //    Console.WriteLine("a=2 b=3");

            //    // 两常量加
            //    var addingResults = session.GetRunner().Run(graph.Add(a, b));
            //    var addingResultValue = addingResults.GetValue();
            //    Console.WriteLine("a+b={0}", addingResultValue);

            //    // 两常量乘
            //    var multiplyResults = session.GetRunner().Run(graph.Mul(a, b));
            //    var multiplyResultValue = multiplyResults.GetValue();
            //    Console.WriteLine("a*b={0}", multiplyResultValue);
            //    var tft = new TFTensor(Encoding.UTF8.GetBytes($"Hello TensorFlow Version {TFCore.Version}! LineZero"));
            //    var hello = graph.Const(tft);
            //    var helloResults = session.GetRunner().Run(hello);
            //    Console.WriteLine(Encoding.UTF8.GetString((byte[])helloResults.GetValue()));
            //}
            Console.ReadKey(); 
        }
    }
}
