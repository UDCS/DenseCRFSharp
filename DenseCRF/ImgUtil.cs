﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DenseCRF
{
  public  class ImgUtil
    {
        /// <summary>
        /// 双线性插值
        /// </summary>
        /// <param name="array">二维数组</param>
        /// <param name="length_0">输出的宽</param>
        /// <param name="length_1">输出的高</param>
        /// <returns></returns>
        public static float[,] BilinearInterp(float[,] array, int length_0, int length_1)
        {
            float[,] _out = new float[length_0, length_1];
            int original_0 = array.GetLength(0);
            int original_1 = array.GetLength(1);

            float ReScale_0 = original_0 / ((float)length_0);  // 倍数的倒数
            float ReScale_1 = original_1 / ((float)length_1);

            float index_0;
            float index_1;
            int inde_0;
            int inde_1;
            float s_leftUp;
            float s_rightUp;
            float s_rightDown;
            float s_leftDown;

            for (int i = 0; i < length_0; i++)
            {
                for (int j = 0; j < length_1; j++)
                {
                    index_0 = i * ReScale_0;
                    index_1 = j * ReScale_1;
                    inde_0 = (int)Math.Floor(index_0);
                    inde_1 = (int)Math.Floor(index_1);
                    s_leftUp = (index_0 - inde_0) * (index_1 - inde_1);
                    s_rightUp = (inde_0 + 1 - index_0) * (index_1 - inde_1);
                    s_rightDown = (inde_0 + 1 - index_0) * (inde_1 + 1 - index_1);
                    s_leftDown = (index_0 - inde_0) * (inde_1 + 1 - index_1);
                    _out[i, j] = array[inde_0, inde_1] * s_rightDown + array[inde_0 + 1, inde_1] * s_leftDown + array[inde_0 + 1, inde_1 + 1] * s_leftUp + array[inde_0, inde_1 + 1] * s_rightUp;
                }
            }

            return _out;
        }

    }
}
